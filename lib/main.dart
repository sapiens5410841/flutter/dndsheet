import 'package:flutter/material.dart';
import 'package:shared_pref_test/Pages/Home.dart';

main (){
  runApp(MaterialApp(title: "Shared Project",
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}