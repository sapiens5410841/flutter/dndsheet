import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController _controllerNome = TextEditingController();
  TextEditingController _controllerClasseNivel = TextEditingController();
  TextEditingController _controllerRaca = TextEditingController();
  TextEditingController _controllerAntecedente = TextEditingController();
  TextEditingController _controllerTendencia = TextEditingController();
  TextEditingController _controllerJogador = TextEditingController();
  TextEditingController _controllerExperiencia = TextEditingController();
  double _sbh = 20.0;

  void _Salvar() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString("nome", _controllerNome.text ?? "");
    await prefs.setString("classeNivel", _controllerClasseNivel.text ?? "");
    await prefs.setString("raca", _controllerRaca.text ?? "");
    await prefs.setString("antecedente", _controllerAntecedente.text ?? "");
    await prefs.setString("tendencia", _controllerTendencia.text ?? "");
    await prefs.setString("jogador", _controllerJogador.text ?? "");
    await prefs.setString("experiencia", _controllerExperiencia.text ?? "");
  }

  void _Carregar() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      _controllerNome.text = prefs.getString("nome") ?? "";
      _controllerClasseNivel.text = prefs.getString("classeNivel") ?? "";
      _controllerRaca.text = prefs.getString("raca") ?? "";
      _controllerAntecedente.text = prefs.getString("antecedente") ?? "";
      _controllerTendencia.text = prefs.getString("tendencia") ?? "";
      _controllerJogador.text = prefs.getString("jogador") ?? "";
      _controllerExperiencia.text = prefs.getString("experiencia") ?? "";
    });
  }

  void _Limpar() {
    setState(() {
      _controllerNome.text = "";
      _controllerClasseNivel.text = "";
      _controllerRaca.text = "";
      _controllerAntecedente.text = "";
      _controllerTendencia.text = "";
      _controllerJogador.text = "";
      _controllerExperiencia.text = "";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("5e Sheet"),
        actions: [
          IconButton(icon: Icon(Icons.save), onPressed: _Salvar),
          IconButton(icon: Icon(Icons.drive_folder_upload_rounded), onPressed: _Carregar),
          IconButton(icon: Icon(Icons.clear), onPressed: _Limpar),
        ],
      ),
      body: ListView(padding: EdgeInsets.all(20),
        children: [
          TextField(keyboardType: TextInputType.text,
            decoration: InputDecoration(labelText: "Nome"),
            controller: _controllerNome,
          ),
          SizedBox(height: _sbh),
          TextField(keyboardType: TextInputType.text,
            decoration: InputDecoration(labelText: "Classe e Nivel"),
            controller: _controllerClasseNivel,
          ),
          SizedBox(height: _sbh),
          TextField(keyboardType: TextInputType.text,
            decoration: InputDecoration(labelText: "Raça"),
            controller: _controllerRaca,
          ),
          SizedBox(height: _sbh),
          TextField(keyboardType: TextInputType.text,
            decoration: InputDecoration(labelText: "Antecedente"),
            controller: _controllerAntecedente,
          ),
          SizedBox(height: _sbh),
          TextField(keyboardType: TextInputType.text,
            decoration: InputDecoration(labelText: "Tendencia"),
            controller: _controllerTendencia,
          ),
          SizedBox(height: _sbh),
          TextField(keyboardType: TextInputType.text,
            decoration: InputDecoration(labelText: "Jogador"),
            controller: _controllerJogador,
          ),
          SizedBox(height: _sbh),
          TextField(keyboardType: TextInputType.number,
            decoration: InputDecoration(labelText: "Experiencia"),
            controller: _controllerExperiencia,
          ),
      ],),
    );
  }
}
